package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectRemoveByIdRequest(@Nullable String token) {
        super(token);
    }

    public ProjectRemoveByIdRequest(@Nullable String token, @Nullable String projectId) {
        super(token);
        this.projectId = projectId;
    }

}

