package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest(@Nullable String token) {
        super(token);
    }

    public UserRegistryRequest(
            @Nullable String token,
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) {
        super(token);
        this.login = login;
        this.password = password;
        this.email = email;
    }

}
