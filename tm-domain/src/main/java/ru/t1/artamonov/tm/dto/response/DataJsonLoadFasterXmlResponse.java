package ru.t1.artamonov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataJsonLoadFasterXmlResponse extends AbstractResponse {
}
