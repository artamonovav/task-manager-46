package ru.t1.artamonov.tm.exception.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException(
            @NotNull final String message
    ) {
        super(message);
    }

    public AbstractSystemException(
            @NotNull final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractSystemException(
            @NotNull final Throwable cause
    ) {
        super(cause);
    }

    public AbstractSystemException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
