package ru.t1.artamonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.model.IWBS;
import ru.t1.artamonov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(length = 100)
    private String name = "";

    @NotNull
    @Column(name = "descrptn", length = 2000)
    private String description = "";

    @NotNull
    @Column(length = 50)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column
    private Date created = new Date();

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

}
