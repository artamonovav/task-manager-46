package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.AbstractModelDTO;
import ru.t1.artamonov.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface IDTORepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void clearAll();

    @NotNull
    Boolean existsById(@NotNull String id);

    @Nullable
    List<M> findAll();

    @Nullable
    List<M> findAll(@NotNull Comparator comparator);

    @Nullable
    List<M> findAll(@NotNull Sort sort);

    @Nullable
    M findOneById(@NotNull String id);

    long getSize();

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void update(@NotNull M model);

}
