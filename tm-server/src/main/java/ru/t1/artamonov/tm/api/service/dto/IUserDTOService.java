package ru.t1.artamonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.enumerated.Role;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IUserDTOService {

    @NotNull
    IUserDTORepository getRepository(@NotNull EntityManager entityManager);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    boolean existsById(@Nullable String id);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull
    UserDTO remove(@Nullable UserDTO user);

    @NotNull
    UserDTO removeById(@Nullable String id);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
