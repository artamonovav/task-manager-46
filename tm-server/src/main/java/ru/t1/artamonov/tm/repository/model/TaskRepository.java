package ru.t1.artamonov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.model.ITaskRepository;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clearAll() {
        @Nullable List<Task> tasks = findAll();
        if (tasks == null) return;
        for (@NotNull Task task : tasks) {
            entityManager.remove(task);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId.isEmpty()) return;
        @Nullable List<Task> tasks = findAllUserId(userId);
        if (tasks == null) return;
        for (@NotNull Task task : tasks) {
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Task m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty()) return false;
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM Task m WHERE m.id = :id and m.user.id = :userId";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @NotNull final String jpql = "SELECT m FROM Task m";
        return entityManager.createQuery(jpql, Task.class).getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final Comparator comparator) {
        @NotNull final String jpql = "SELECT m FROM Task m ORDER BY m."
                + getSortType(comparator);
        return entityManager.createQuery(jpql, Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final Sort sort) {
        @NotNull final String jpql = "SELECT m FROM Task m ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllUserId(@Nullable final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class).
                setParameter("userId", userId)
                .getResultList();
    }


    @Nullable
    @Override
    public List<Task> findAllUserId(@NotNull final String userId, @NotNull final Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllUserId(@NotNull final String userId, @NotNull final Comparator comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId ORDER BY m."
                + getSortType(comparator);
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId.isEmpty()) return Collections.emptyList();
        if (projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId and m.project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        if (projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Nullable
    @Override
    public Task findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.id = :id AND m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getSizeUserId(@Nullable final String userId) {
        if (userId.isEmpty()) return 0;
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable Task task = findOneById(id);
        if (task != null) entityManager.remove(task);
    }

    @Override
    public void removeByIdUserId(@Nullable final String userId, @Nullable final String id) {
        @Nullable Task task = findOneByIdUserId(userId, id);
        if (task != null) entityManager.remove(task);
    }

    @Override
    public void removeTasksByProjectId(@NotNull final String projectId) {
        @Nullable List<Task> tasks = findAllByProjectId(projectId);
        if (tasks == null) return;
        for (@NotNull Task task : tasks) {
            entityManager.remove(task);
        }
    }

}
