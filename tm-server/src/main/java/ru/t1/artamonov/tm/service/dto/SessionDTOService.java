package ru.t1.artamonov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.dto.ISessionDTOService;
import ru.t1.artamonov.tm.dto.model.SessionDTO;
import ru.t1.artamonov.tm.exception.entity.ModelNotFoundException;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.exception.user.AccessDeniedException;
import ru.t1.artamonov.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionDTOService implements ISessionDTOService {

    private IConnectionService connectionService;

    public SessionDTOService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    @Override
    public ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            model.setUserId(userId);
            entityManager.getTransaction().begin();
            sessionRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<SessionDTO> add(@NotNull List<SessionDTO> models) {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull SessionDTO model : models) {
                sessionRepository.add(model);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public @Nullable List<SessionDTO> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            return sessionRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            return sessionRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            return sessionRepository.findOneByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO update(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO remove(@Nullable SessionDTO model) {
        if (model == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(id);
        if (session == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull SessionDTO removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(userId, id);
        if (session == null) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
