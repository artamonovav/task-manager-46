package ru.t1.artamonov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.artamonov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.artamonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.artamonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.artamonov.tm.exception.field.TaskIdEmptyException;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.repository.dto.ProjectDTORepository;
import ru.t1.artamonov.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskDTOService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    @Override
    public ITaskDTORepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @NotNull
    @Override
    public IProjectDTORepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository(entityManager);

        try {
            if (!projectRepository.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDTO task = Optional.ofNullable(taskRepository.findOneByIdUserId(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeTasksByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository(entityManager);
        try {
            if (!projectRepository.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDTO task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
