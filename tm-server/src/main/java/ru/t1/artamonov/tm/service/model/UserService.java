package ru.t1.artamonov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.model.IUserRepository;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.api.service.model.IUserService;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.exception.entity.UserNotFoundException;
import ru.t1.artamonov.tm.exception.field.EmailEmptyException;
import ru.t1.artamonov.tm.exception.field.IdEmptyException;
import ru.t1.artamonov.tm.exception.field.LoginEmptyException;
import ru.t1.artamonov.tm.exception.field.PasswordEmptyException;
import ru.t1.artamonov.tm.exception.user.ExistsEmailException;
import ru.t1.artamonov.tm.exception.user.ExistsLoginException;
import ru.t1.artamonov.tm.model.User;
import ru.t1.artamonov.tm.repository.model.UserRepository;
import ru.t1.artamonov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @Override
    public UserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    @Override
    public User create(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            @NotNull User user = new User();
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(Role.USUAL);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            @NotNull User user = new User();
            user.setLogin(login);
            user.setEmail(email);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(Role.USUAL);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable String login, @Nullable String password, @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        boolean loginExist = isLoginExist(login);
        if (loginExist) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            @NotNull User user = new User();
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            if (role == null) user.setRole(Role.USUAL);
            else user.setRole(role);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            return userRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            return userRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            return userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExist(@Nullable String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public @NotNull User remove(@Nullable User user) {
        if (user == null) throw new UserNotFoundException();
        return removeById(user.getId());
    }

    @Override
    public @NotNull User removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<User> set(@NotNull Collection<User> users) {
        if (users == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull User user : users)
                userRepository.add(user);
            entityManager.getTransaction().commit();
            return users;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User setPassword(@Nullable String id, @Nullable String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
